FROM python:3.8-slim

WORKDIR /srv/petvet

# Install dependencies only if they changed (leverage Docker layers)
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY clinic .

# Prepare entrypoint script
COPY docker-entrypoint.sh .
ENTRYPOINT ["./docker-entrypoint.sh"]

CMD ["gunicorn", "clinic.wsgi"]

# Tell users on which port the container listens
EXPOSE 9000
